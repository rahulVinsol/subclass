class Vehicle {
	let name: String
	var price: Double
	
	init(name: String, price: Double) {
		self.name = name
		self.price = price
	}
	
	func showContents() {
		print(
		"""
		name   -> \(name)
		price  -> \(price)
		"""
		)
	}
	
	func increasePrice(increaseBy: Double = 10000) {
		self.price += increaseBy
	}
}

class Bike: Vehicle {
	let dealer: String
	
	init(name: String, price: Double, dealer: String) {
		self.dealer = dealer
		super.init(name: name, price: price)
	}
	
	override func showContents() {
		super.showContents()
		print(
		"""
		dealer -> \(dealer)
		"""
		)
	}
}

let bike = Bike(name: "Yamaha YZF R15 V3", price: 150000.0, dealer: "zigwheels")
bike.showContents()

print()

bike.increasePrice()
bike.showContents()
